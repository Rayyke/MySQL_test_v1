<?php
    include ($_SERVER['DOCUMENT_ROOT'].'/z2/config.php');
    include ($_SERVER['DOCUMENT_ROOT'].'/z2/scripts/php/tableDisplay.php');

    header('Content-Type: text/html; charset=utf-8');

    // input from new user in #addUserForm
    $newName = $_REQUEST['newName'];
    $newSurname = $_REQUEST['newSurname'];
    $newBirthDay = $_REQUEST['newBirthDay'];
    $newBirthPlace = $_REQUEST['newBirthPlace'];
    $newBirthCountry = $_REQUEST['newBirthCountry'];
    $newDeathDay = $_REQUEST['newDeathDay'];
    $newDeathPlace = $_REQUEST['newDeathPlace'];
    $newDeathCountry = $_REQUEST['newDeathCountry'];

    // input from new event in #addEventForm
    $addEventInput1 = $_REQUEST['addEventInput1'];
    $addEventInput2 = $_REQUEST['addEventInput2'];
    $addEventInput3 = $_REQUEST['addEventInput3'];
    $addEventInput4 = $_REQUEST['addEventInput4'];

    // input from editing event in #editEvent
    $newMedal = $_REQUEST['newMedal'];
    $newDiscipline = $_REQUEST['newDiscipline']; 

    // maintain the chosen order which user requested in #mainTable
    $currentOrderBy = $_REQUEST['currentOrderBy'];
    $currentTypeOfOrderBy = $_REQUEST['currentTypeOfOrderBy'];

    $button = $_REQUEST['button']; // combination of type of button and its unique id (usually looks like i.e.: e17)
    $ID_event = substr($button, 1); // id which is user targeting
    $type = $button[0];

    /* Explanation of $type:
        d - User is deleting event from ................... #mainTable
        e - User is clicking edit event in ................ #mainTable
        c - User is confirmating edit event in pop up ..... #editEvent
        u - User is chcecking profile in .................. #mainTable
        n - User is confirmating new profile in pop up .... #addUserForm
        r - User is deleting profile in pop up ............ #user
        v - User is clicking new event in ................. #menu
        t - user is confirmating new event in pop up ...... #addEventForm
    */
    
    try{
        $dbh = new PDO("mysql:host=$hostname; dbname=$dbname", $username, $password);
        $dbh->exec("SET CHARACTER SET " . $charset);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        
        if($type == "d"){
            $sql = "DELETE FROM Umiestnenia
                    WHERE id=" . $ID_event . ";";
            
            $dbh->query($sql);
            tableDisplay($dbh, $currentOrderBy, $currentTypeOfOrderBy);
        }
        else if($type == "e"){
            $sql = "SELECT  discipline, 
                            place, 
                            id
                    FROM Umiestnenia
                    WHERE id = " . $ID_event . ";";

            foreach ($dbh->query($sql) as $row){
                print '<form">' .
                        'Place: <input id="editEventPlace" class="editEventShort" type="text" name="Place" value="' . $row['place'] . '"> | ' .
                        'Discipline: <input id="editEventDiscipline" class="editEventLong" type="text" name="Discipline" value="' . $row['discipline'] . '">' .
                        '<img class="confirmEditButton" src="pics/confirmEditButton.png" alt="OK">' .
                        '<img class="discardButton" src="pics/discardButton.png" alt="X">' .
                    '</form>';       
            }
        }
        else if($type == "c"){
            $sql = "UPDATE Umiestnenia 
                SET place = " . $newMedal . ", discipline = '" . $newDiscipline . 
                "' WHERE id=" . $ID_event . ";";
        
            $dbh->query($sql);
            tableDisplay($dbh, $currentOrderBy, $currentTypeOfOrderBy);
        }
        else if($type == "u"){
            $sql = "SELECT id, name, surname, birthDay, birthPlace, birthCountry, deathDay, deathPlace, deathCountry 
                    FROM Osoby
                    WHERE id = " . $ID_event . ";";

            foreach ($dbh->query($sql) as $row){
                print '<img id="exitCross" src="pics/exit.png" alt="X">';
                print '<img class="binButton" id="binButton' . $row['id'] . '" src="pics/bin.png" alt="Delete">';

                print '<table cellspacing="0" cellpadding="0"><tr>';
                print "<td>Id: </td><td>" . $row['id'] . "</td>";
                print '</tr><tr>';
                print "<td>Name: </td><td>" . $row['name'] . "</td>";
                print '</tr><tr>';
                print "<td>Surname: </td><td>" . $row['surname'] . "</td>";
                print '</tr><tr>';
                print "<td>Birth day: </td><td>" . $row['birthDay'] . "</td>";
                print '</tr><tr>';
                print "<td>Birth place: </td><td>" . $row['birthPlace'] . "</td>";
                print '</tr><tr>';
                print "<td>Birth country: </td><td>" . $row['birthCountry'] . "</td>";
                print '</tr>';

                if(!empty($row['deathDay'])){
                    print '<tr>';
                    print "<td>Death day: </td><td>" . $row['birthDay'] . "</td>";
                    print '</tr><tr>';
                    print "<td>Death place: </td><td>" . $row['birthPlace'] . "</td>";
                    print '</tr><tr>';
                    print "<td>Death country: </td><td>" . $row['birthCountry'] . "</td>";    
                    print '</tr>';
                }
                
                print "</table>";
            }

            print '<hr style="  border-width: 1px; 
                                margin-top: 15px;
                                margin-bottom: 5px;
                                width: 97.5%">';

            print '<table cellspacing="0" cellpadding="0">';

            $sql = "SELECT  Osoby.name AS 'FirstName', 
                        Osoby.surname AS 'Surname', 
                        OH.year AS 'Year', 
                        OH.city AS 'City', 
                        OH.country AS 'Country', 
                        OH.type AS 'Type', 
                        Umiestnenia.discipline AS 'Discipline', 
                        Umiestnenia.place AS 'Place', 
                        Umiestnenia.id AS 'ID_event',
                        Osoby.id AS 'ID_user'
                FROM ((Osoby 
                JOIN Umiestnenia ON Osoby.id = Umiestnenia.id_person)
                JOIN OH ON OH.id = Umiestnenia.id_OH)
                WHERE Osoby.id = " . $ID_event . ";";

            foreach ($dbh->query($sql) as $row){
                print '<tr>';
                print '<td class="medal">'; 

                if((int)$row['Place'] <= 3){
                    print '<img class="medal" src="pics/medal' . $row['Place'] . '.png" alt="' . $row['Place'] . '">';
                }
                else{
                    print $row['Place'];
                }
        
                print '</td><td>' . $row['Year'] . '</td><td>' 
                                    . $row['City'] . '</td><td>' 
                                    . $row['Country'] . '</td><td>' 
                                    . $row['Type'] . '</td><td class="discipline">' 
                                    . $row['Discipline'] . '</td>';
                print '</tr>';
            }

            print '</table>';
        }
        else if($type == "n"){
            // counting total IDs, in case there is no gap
            $sql = "SELECT COUNT(id) AS 'total' FROM Osoby;";

            foreach ($dbh->query($sql) as $row){
                $newId = (int)$row['total'] + 1;
            }

            // calculating first available unique user ID
            $prevId = 0;
            $sql = "SELECT id FROM Osoby";
            
            foreach ($dbh->query($sql) as $row){
                if($row['id'] - $prevId > 1){
                    $newId = $prevId + 1; // if there is gap between IDs, go for next available
                    break;
                }
                else{
                    $prevId = (int)$row['id'];
                }
            }

            if($newDeathDay != "" && $newDeathPlace != "" && $newDeathCountry != ""){
                $sql = "INSERT INTO Osoby (id, name, surname, birthDay, birthPlace, birthCountry, deathDay, deathPlace, deathCountry) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            }
            else if($newDeathDay == "" && $newDeathPlace == "" && $newDeathCountry == ""){
                $sql = "INSERT INTO Osoby (id, name, surname, birthDay, birthPlace, birthCountry) VALUES (?, ?, ?, ?, ?, ?)";    
            }
            
            if($newName != "" && $newSurname != "" && $newBirthDay != "" && $newBirthPlace != "" && $newBirthCountry != ""){
                $statement = $dbh->prepare($sql);
            }

            if($newDeathDay != "" && $newDeathPlace != "" && $newDeathCountry != ""){
                if($newName != "" && $newSurname != "" && $newBirthDay != "" && $newBirthPlace != "" && $newBirthCountry != ""){
                    $statement->execute(array($newId, $newName,$newSurname,$newBirthDay,$newBirthPlace,$newBirthCountry,$newDeathDay,$newDeathPlace, $newDeathCountry));
                }  
            }
            else if($newDeathDay == "" && $newDeathPlace == "" && $newDeathCountry == ""){
                if($newName != "" && $newSurname != "" && $newBirthDay != "" && $newBirthPlace != "" && $newBirthCountry != ""){
                    $statement->execute(array($newId, $newName,$newSurname,$newBirthDay,$newBirthPlace,$newBirthCountry));   
                }  
            }
        }
        else if($type == "r"){
            $sql = "DELETE FROM Osoby
                    WHERE id = " . $ID_event . ";";
            
            $dbh->query($sql);

            $sql = "DELETE FROM Umiestnenia
                    WHERE id_person = " . $ID_event . ";";
            
            $dbh->query($sql);

            tableDisplay($dbh, $currentOrderBy, $currentTypeOfOrderBy);
        }
        else if($type == "v"){
            print '<table cellspacing="0" cellpadding="0"><tr><td>Full name</td><td><select id="addEventInput1">';

            $sql = "SELECT id, name, surname FROM Osoby";   
            
            foreach ($dbh->query($sql) as $row){
                print '<option value="' . $row['id'] . '">';
                print $row['name'] . ' ' . $row['surname'];
                print '</option>';
            }

            print '</select></td></tr><tr><td>Olympiade</td><td><select id="addEventInput2">';

            $sql = "SELECT id, type, year, city, country FROM OH";   
            
            foreach ($dbh->query($sql) as $row){
                print '<option value="' . $row['id'] . '">';
                print $row['type'] . ', ' . $row['year'] . ', ' . $row['city'] . ', ' . $row['country'];
                print '</option>';
            }

            print '</select></td></tr><tr><td>Place</td><td>';
        
            print '<input type="number" id="addEventInput3"></td></tr>';
            print '<r><td>Discipline</td><td><input type="text" id="addEventInput4"></td></tr></table>';

            print '<hr style="  border-width: 1px; 
                                margin-top: 15px;
                                margin-bottom: 5px;
                                width: 95%">';

            print '<div class="newFormButtons">
                        <img class="confirmNewEventButton" src="pics/confirmEditButton.png" alt="OK">
                        <img class="discardNewEventButton" src="pics/discardButton.png" alt="X">
                    </div>';
        }
        else if($type == "t"){
            // counting total IDs, in case there is no gap
            $sql = "SELECT COUNT(id) AS 'total' FROM Umiestnenia;";

            foreach ($dbh->query($sql) as $row){
                $newId = (int)$row['total'] + 1;
            }

            // calculating first available unique event ID
            $prevId = 0;
            $sql = "SELECT id FROM Umiestnenia";
            
            foreach ($dbh->query($sql) as $row){
                if($row['id'] - $prevId > 1){
                    $newId = $prevId + 1; // if there is gap between IDs, go for next available
                    break;
                }
                else{
                    $prevId = (int)$row['id'];
                }
            }

            $sql = "INSERT INTO Umiestnenia(id, id_person, id_OH, place, discipline) 
                    VALUES(?, ?, ?, ?, ?);";

            $statement = $dbh->prepare($sql);

            $statement->execute(array($newId, $addEventInput1, $addEventInput2, $addEventInput3, $addEventInput4));
            tableDisplay($dbh, $currentOrderBy, $currentTypeOfOrderBy);
        }
        
        $dbh = null;
    }
    catch(PDOException $e){
        echo 'Database connection check: ERR(' . $e->getMessage() . ')';
    }
?> 