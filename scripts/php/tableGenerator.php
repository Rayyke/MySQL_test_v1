<?php
    include ($_SERVER['DOCUMENT_ROOT'].'/z2/config.php');
    include ($_SERVER['DOCUMENT_ROOT'].'/z2/scripts/php/tableDisplay.php');

    header('Content-Type: text/html; charset=utf-8');

    // catching input parameter from client
    $currentOrderBy = $_REQUEST['currentOrderBy'];
    $currentTypeOfOrderBy = $_REQUEST['currentTypeOfOrderBy'];

    try{
        $dbh = new PDO("mysql:host=$hostname; dbname=$dbname", $username, $password);
        $dbh->exec("SET CHARACTER SET " . $charset);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

        // if $currentOrderBy has "-1" value, it´s only testing database connection
        if($currentOrderBy != "-1"){
            tableDisplay($dbh, $currentOrderBy, $currentTypeOfOrderBy);
        }
        else if($currentOrderBy == "-1"){
            print 'Database connection check: OK';
        }
        
        $dbh = null;
    }
    catch(PDOException $e){
        echo 'Database connection check: ERR(' . $e->getMessage() . ')';
    }
?> 