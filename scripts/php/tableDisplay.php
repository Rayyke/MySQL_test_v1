<?php
    // function displays data to table (provides table in HTML form)
    function tableDisplay($dbh, $currentOrderBy, $currentTypeOfOrderBy){
        $sql = "SELECT  Osoby.name AS 'FirstName', 
                        Osoby.surname AS 'Surname', 
                        OH.year AS 'Year', 
                        OH.city AS 'City', 
                        OH.country AS 'Country', 
                        OH.type AS 'Type', 
                        Umiestnenia.discipline AS 'Discipline', 
                        Umiestnenia.place AS 'Place', 
                        Umiestnenia.id AS 'ID_event',
                        Osoby.id AS 'ID_user'
                FROM ((Osoby 
                JOIN Umiestnenia ON Osoby.id = Umiestnenia.id_person)
                JOIN OH ON OH.id = Umiestnenia.id_OH)
                WHERE Place <= 3";

        // add ORDER BY something, if there is valid paramter
        if($currentOrderBy != "0"){
        $sql = $sql . " ORDER BY " . $currentOrderBy . ' ' . $currentTypeOfOrderBy;
        }

        // ordering by 2nd parameter year, if im ordering by type
        if($currentOrderBy == "Type"){
        $sql = $sql . ", Year " . $currentTypeOfOrderBy;    
        }

        // simply right syntax :)
        $sql = $sql . ";";

        // quering, parsing SQL and sending html back to client in a form of table
        foreach ($dbh->query($sql) as $row){
            print '<tr>';
            print   '<td class="medal">'  
                . '<img class="medal" src="pics/medal' . $row['Place'] . '.png" alt="' . $row['Place'] . '"></td><td>'  
                . $row['FirstName'] . '</td><td>' 
                . $row['Surname'] . '</td><td>' 
                . $row['Year'] . '</td><td>' 
                . $row['City'] . '</td><td>' 
                . $row['Country'] . '</td><td>' 
                . $row['Type'] . '</td><td class="discipline">' 
                . $row['Discipline'] . '</td><td class="buttons">'
                . '<img id="u' . $row['ID_user'] . '" class="button" src="pics/displayUser.png" alt="u">&nbsp;&nbsp;
                    <img id="d' . $row['ID_event'] . '" class="button" src="pics/delete.png" alt="d">&nbsp;&nbsp;
                    <img id="e' . $row['ID_event'] . '" class="button" src="pics/edit.png" alt="e"></td>';
            print '</tr>';
        }
    }
?>