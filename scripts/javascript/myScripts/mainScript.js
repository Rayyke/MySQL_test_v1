var currentOrderBy = "0"; // Name, Surname, Year, ...
var currentTypeOfOrderBy = "0"; // DESC, ASC
var transitionTime = 200; // transition face timer

$(document).ready(function(){
    // checking connection on database, check console log for status
    $.ajax({
        url: 'scripts/php/tableGenerator.php',
        type: "POST",
        data: ({currentOrderBy: "-1"}),
        success: function(data){
            console.log(data);
        }
    });

    // default filling of main table, not ordered at all
    $.ajax({
        url: 'scripts/php/tableGenerator.php',
        type: "POST",
        data: ({currentOrderBy: "0"}),
        success: function(data){
            $("#mainTable").html(data);
            bindButtons();
        }
    });

    // adding listeners to all clickables in the head of table
    $("div.sortClickers").click(function(){
        /*  If same element is clicked again, changing ASC and DESC.
            If I choose different element, I am automatically setting ASC by default.
        */
        if(currentOrderBy == $(this).attr('id')){
            if(currentTypeOfOrderBy == "ASC"){
                currentTypeOfOrderBy = "DESC";
            }
            else{
                currentTypeOfOrderBy = "ASC";    
            }
        }
        else{
            currentTypeOfOrderBy = "ASC";
            currentOrderBy = $(this).attr('id');    
        }
        
        $.ajax({
            url: 'scripts/php/tableGenerator.php',
            type: "POST",
            data: ({currentOrderBy: currentOrderBy, currentTypeOfOrderBy: currentTypeOfOrderBy}),
            success: function(data){
                $("#mainTable").html(data);
                bindButtons();
            }
        });

        if($("#main2").scrollTop() != 0){
            $("#main2").animate({
                scrollTop: 0
            }, 'slow');
        }
    }).css('cursor', 'pointer');

    $("#addUser").click(function(){
        fadeShow("#fullScreenFill");
        fadeShow("#addUserForm");      
    }).css('cursor', 'pointer');

    $("#addEvent").click(function(){
        $.ajax({
            url: 'scripts/php/dataManager.php',
            type: "POST",
            data: ({button: "v"}),
            success: function(data){
                $("#addEventForm").html(data);

                $(".confirmNewEventButton").click(function(){
                    $.ajax({
                        url: 'scripts/php/dataManager.php',
                        type: "POST",
                        data: ({button: "t",
                                addEventInput1: $("#addEventInput1").val(), 
                                addEventInput2: $("#addEventInput2").val(),
                                addEventInput3: $("#addEventInput3").val(), 
                                addEventInput4: $("#addEventInput4").val(),
                                currentOrderBy: currentOrderBy, 
                                currentTypeOfOrderBy: currentTypeOfOrderBy
                                }),
                        success: function(data){
                            $("#mainTable").html(data);
                            bindButtons(); 

                            fadeHide("#fullScreenFill");
                            fadeHide("#addEventForm");
            
                            $("#addEventInput3").val("");  
                            $("#addEventInput4").val("");         
                        }
                    });   
                }).css('cursor', 'pointer');

                $(".discardNewEventButton").click(function(){
                    fadeHide("#fullScreenFill");
                    fadeHide("#addEventForm");
                }).css('cursor', 'pointer');
            }
        });

        fadeShow("#fullScreenFill");
        fadeShow("#addEventForm");
    }).css('cursor', 'pointer');

    $("#fullScreenFill").click(function(){
        fadeHide("#fullScreenFill");
        fadeHide("#addUserForm");
        fadeHide("#addEventForm");
        fadeHide("#user");
        fadeHide("#editEvent");
    });

    $(".confirmNewUserButton").click(function(){
        $.ajax({
            url: 'scripts/php/dataManager.php',
            type: "POST",
            data: ({button: "n",
                    newName: $("#newName").val(), 
                    newSurname: $("#newSurname").val(), 
                    newBirthDay: $("#newBirthDay").val(), 
                    newBirthPlace: $("#newBirthPlace").val(), 
                    newBirthCountry: $("#newBirthCountry").val(), 
                    newDeathDay: $("#newDeathDay").val(), 
                    newDeathPlace: $("#newDeathPlace").val(), 
                    newDeathCountry: $("#newDeathCountry").val()}),
            success: function(data){
                fadeHide("#fullScreenFill");
                fadeHide("#addUserForm");

                $("#newName").val("");  
                $("#newSurname").val("");  
                $("#newBirthDay").val("");  
                $("#newBirthPlace").val("");  
                $("#newBirthCountry").val("");  
                $("#newDeathDay").val("");  
                $("#newDeathPlace").val("");  
                $("#newDeathCountry").val("");        
            }
        });   
    }).css('cursor', 'pointer');

    $(".discardNewUserButton").click(function(){
        fadeHide("#fullScreenFill");
        fadeHide("#addUserForm");
    }).css('cursor', 'pointer');
});

// function re-bind new generated buttons in #mainTable (all 3 buttons in the very last column)
function bindButtons(){
    $(".button").click(function(){
        var temp = $(this).attr('id');

        $.ajax({
            url: 'scripts/php/dataManager.php',
            type: "POST",
            data: ({button: temp, currentOrderBy: currentOrderBy, currentTypeOfOrderBy: currentTypeOfOrderBy}),
            success: function(data){
                // action when it is delete event button
                if(temp.charAt(0) == "d"){
                    $("#mainTable").html(data);
                    bindButtons();
                }
                // action when it is edit event button
                else if(temp.charAt(0) == "e"){
                    fadeShow("#fullScreenFill");
                    fadeShow("#editEvent");

                    $("#editEvent").html(data);

                    $(".confirmEditButton").click(function(){
                        newMedal = $("#editEventPlace").val();
                        newDiscipline = $("#editEventDiscipline").val();
                        
                        temp = "c" + temp.substr(1);

                        $.ajax({
                            url: 'scripts/php/dataManager.php',
                            type: "POST",
                            data: ({button: temp, currentOrderBy: currentOrderBy, currentTypeOfOrderBy: currentTypeOfOrderBy, 
                                    newMedal: newMedal, newDiscipline: newDiscipline}),
                            success: function(data){
                                $("#mainTable").html(data);
                                bindButtons();       
                            }
                        });
                        
                        fadeHide("#fullScreenFill");
                        fadeHide("#editEvent");
                    }).css('cursor', 'pointer');

                    $(".discardButton").click(function(){
                        fadeHide("#fullScreenFill");
                        fadeHide("#editEvent");
                    }).css('cursor', 'pointer');
                }
                // action when it is display user button
                else if(temp.charAt(0) == "u"){
                    fadeShow("#fullScreenFill");
                    fadeShow("#user");

                    $("#user").html(data);    

                    $("#exitCross").click(function(){
                        fadeHide("#fullScreenFill");
                        fadeHide("#user");
                    }).css('cursor', 'pointer');

                    
                    $(".binButton").click(function(){
                        var temp = "r" + ($(this).attr('id')).substr(9);

                        $.ajax({
                            url: 'scripts/php/dataManager.php',
                            type: "POST",
                            data: ({button: temp, currentOrderBy: currentOrderBy, currentTypeOfOrderBy: currentTypeOfOrderBy}),
                            success: function(data){
                                $("#mainTable").html(data);
                                bindButtons();       
                            }
                        });

                        fadeHide("#fullScreenFill");
                        fadeHide("#user");
                    }).css('cursor', 'pointer');
                }
            }
        }); 
    }).css('cursor', 'pointer');
}

function fadeHide(hide){
    $(hide).fadeTo(transitionTime, 0, function(){
        $(this).css("display", "none");
    });
}

function fadeShow(show){
    $(show).css("display", "block").fadeTo(transitionTime, 1);
}