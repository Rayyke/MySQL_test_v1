<!DOCTYPE html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="pics/favicon.png">

        <!-- my styles-->
        <link rel="stylesheet" type="text/css" href="styles/mainStyle.css">

        <!-- jQuery -->
        <script src="scripts/javascript/jQuery/jquery-3.2.1.min.js"></script>

        <!-- my scripts-->
        <script src="scripts/javascript/myScripts/mainScript.js"></script>
        
        <title>MySQL</title>
    </head>

    <body>
        <header>
            <h1>empty</h1> <!-- umlčuje validátor-->
        </header>
    
        <article>
            <h2>empty</h2> <!-- umlčuje validátor-->
            
            <div id="main">
                <div id="main1">
                    <table>
                        <tr>
                            <td class="heading medal"><div class="sortClickers" id="Place"><img class="medal" src="pics/medal1.png" alt="medal"><img class="medal" src="pics/medal2.png" alt="medal"><img class="medal" src="pics/medal3.png" alt="medal"></div></td>
                            <td class="heading"><div class="sortClickers" id="Name">Name</div></td>
                            <td class="heading"><div class="sortClickers" id="Surname">Surname</div></td>
                            <td class="heading"><div class="sortClickers" id="Year">Year</div></td>
                            <td class="heading"><div class="sortClickers" id="City">City</div></td>
                            <td class="heading"><div class="sortClickers" id="Country">Country</div></td>
                            <td class="heading"><div class="sortClickers" id="Type">Type</div></td>
                            <td class="heading discipline"><div class="sortClickers" id="Discipline">Discipline</div></td>
                            <td class="heading buttonsPlus20px"></td>
                        </tr>
                    </table>
                </div>

                <div id="main2">
                    <table id="mainTable"></table>
                </div>
            </div>

            <div id="menu">
                <img id="addUser" src="pics/addUser.png" alt="addUser">
                <img id="addEvent" src="pics/addEvent.png" alt="addEvent">
            </div>

            <div id="fullScreenFill"></div>

            <div id="editEvent"></div>

            <div id="user"></div>  
            
            <div id="addUserForm">
                <table>
                    <tr>
                        <td>Name: </td>
                        <td><input id="newName" type="text"></td>
                    </tr>

                    <tr>
                        <td>Surname: </td>
                        <td><input id="newSurname" type="text"></td>
                    </tr>

                    <tr>
                        <td>Birth day: </td>
                        <td><input id="newBirthDay" type="text"></td>
                    </tr>

                    <tr>
                        <td>Birth place: </td>
                        <td><input id="newBirthPlace" type="text"></td>
                    </tr>

                    <tr>
                        <td>Birth country: </td>
                        <td><input id="newBirthCountry" type="text"></td>
                    </tr>

                    <tr>
                        <td>Death day: </td>
                        <td><input id="newDeathDay" type="text"></td>
                    </tr>

                    <tr>
                        <td>Death place: </td>
                        <td><input id="newDeathPlace" type="text"></td>
                    </tr>

                    <tr>
                        <td>Death country: </td>
                        <td><input id="newDeathCountry" type="text"></td>
                    </tr>
                </table>

                <hr style="  border-width: 1px; 
                                margin-top: 15px;
                                margin-bottom: 5px;
                                width: 92%"> 
                   
                <div class="newFormButtons">
                    <img class="confirmNewUserButton" src="pics/confirmEditButton.png" alt="OK">
                    <img class="discardNewUserButton" src="pics/discardButton.png" alt="X">
                </div>
            </div> 

            <div id="addEventForm"></div>
        </article>
        
        <footer>
            Peter Čižmár, &copy; 2018 
        </footer>
    </body>
</html>